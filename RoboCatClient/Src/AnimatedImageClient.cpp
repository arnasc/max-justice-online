#include <RoboCatClientPCH.h>

AnimatedImageClient::AnimatedImageClient(ImageClient::Textures whichTexture, int numFramesX, int numFramesY)
{
	mSpriteComponent.reset(new AnimatedSpriteComponent(this, numFramesX, numFramesY));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture(ImageClient::textureMap_[whichTexture]));
	SetScale(.3F);
	mDuration = 1.F;
	mTimeSinceLastFrame = 0;
	mNumOfFrames = numFramesX*numFramesY;
	mTimePerFrame = mDuration / mNumOfFrames;
}

AnimatedImageClient::AnimatedImageClient(ImageClient::Textures whichTexture, int numFramesX, int numFramesY, float duration)
{
	mSpriteComponent.reset(new AnimatedSpriteComponent(this, numFramesX, numFramesY));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture(ImageClient::textureMap_[whichTexture]));
	SetScale(.3F);
	mDuration = duration;
	mTimeSinceLastFrame = 0;
	mNumOfFrames = numFramesX*numFramesY;
	mTimePerFrame = mDuration / mNumOfFrames;
}

AnimatedImageClient::~AnimatedImageClient()
{
	mSpriteComponent->~AnimatedSpriteComponent();
}

void AnimatedImageClient::Update()
{
	if (mSpriteComponent->finished) {
		return;
	}
	float deltaTime = Timing::sInstance.GetDeltaTime();

	mTimeSinceLastFrame += deltaTime;

	

	if (mTimeSinceLastFrame >= mTimePerFrame) {
		mTimeSinceLastFrame = 0;
		mSpriteComponent->NextFrame();
	}
}