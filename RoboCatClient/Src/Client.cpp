#include <RoboCatClientPCH.h>

bool Client::StaticInit( )
{
	// Create the Client pointer first because it initializes SDL
	Client* client = new Client();

	if( WindowManager::StaticInit() == false )
	{
		return false;
	}
	
	if( GraphicsDriver::StaticInit( WindowManager::sInstance->GetMainWindow() ) == false )
	{
		return false;
	}

	SoundManager::StaticInit();
	TextureManager::StaticInit();
	RenderManager::StaticInit();
	InputManager::StaticInit();

	HUD::StaticInit();

	Vector3 center(0.f, 0.f, 0.f);
	GameObjectPtr go = ImageClient::StaticCreate(ImageClient::Textures::Map);
	go->SetLocation(center);
	go->SetCollisionRadius(0);
	World::sInstance->AddGameObject(go);

	GameObjectPtr go1 = ImageClient::StaticCreate(ImageClient::Textures::Van);
	go1->SetLocation(Vector3(1,1,0));
	go1->SetCollisionRadius(0);
	go1->SetRotation(RoboMath::GetRandomFloat() * 360);
	World::sInstance->AddGameObject(go1);

	//Vector3 lampPos(5.f, 0.f, 0.f);
	//GameObjectPtr goLamp = DestructibleClient::StaticCreateLampPost();
	//goLamp->SetLocation(lampPos);
	//World::sInstance->AddGameObject(goLamp);

	/*ector3 lampPos(5.f, 0.f, 0.f);
	GameObjectPtr goLamp = GameObjectRegistry::sInstance->CreateGameObject('LAMP');
	goLamp->SetLocation(lampPos);
	World::sInstance->AddGameObject(goLamp);*/

	sInstance.reset( client );

	SoundManager::sInstance->PlayMusic();

	//GameObjectPtr go1 = AnimatedImageClient::StaticCreate(ImageClient::Textures::Explosion, 4, 4);
	//World::sInstance->AddGameObject(go1);

	//GameObjectPtr go2 = AnimatedImageClient::StaticCreate(ImageClient::Textures::JusticePop, 4, 3);
	//World::sInstance->AddGameObject(go2);

	return true;
}

Client::Client()
{
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RCAT', RoboCatClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RJUS', JusticeClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'YARN', YarnClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'AVAN', VanClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'VANS', ImageClient::StaticCreateVan);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'EXPL', AnimatedImageClient::StaticCreateExplosion );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'POPT', AnimatedImageClient::StaticCreateJusticePop );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'DEST', AnimatedImageClient::StaticCreateDestruction);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'SKDL', SkidClientLeft::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'SKDR', SkidClientRight::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'DSTR', DestructibleClient::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'COLL', CollidableClient::StaticCreate);

	string destination = StringUtils::GetCommandLineArg( 1 );
	string name = StringUtils::GetCommandLineArg( 2 );

	SocketAddressPtr serverAddress = SocketAddressFactory::CreateIPv4FromString( destination );

	NetworkManagerClient::StaticInit( *serverAddress, name, 0);

	//NetworkManagerClient::sInstance->SetDropPacketChance( 0.6f );
	//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.25f );
	//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.5f );
	//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.1f );
}



void Client::DoFrame()
{
	InputManager::sInstance->Update();

	Engine::DoFrame();

	NetworkManagerClient::sInstance->ProcessIncomingPackets();

	RenderManager::sInstance->Render();

	NetworkManagerClient::sInstance->SendOutgoingPackets();
}

void Client::HandleEvent( SDL_Event* inEvent )
{
	switch( inEvent->type )
	{
	case SDL_KEYDOWN:
		InputManager::sInstance->HandleInput( EIA_Pressed, inEvent->key.keysym.sym );
		break;
	case SDL_KEYUP:
		InputManager::sInstance->HandleInput( EIA_Released, inEvent->key.keysym.sym );
		break;
	default:
		break;
	}
}

