#include <RoboCatClientPCH.h>
#include <map>

ImageClient::TextureMap ImageClient::textureMap_ = {
	{ Textures::Map,			"background" },
	{ Textures::Explosion,		"explosion" },
	{ Textures::JusticePop,		"justicepop" },
	{ Textures::SkidMarkLeft,	"skidmarkleft" },
	{ Textures::SkidMarkRight,	"skidmarkright" },
	{ Textures::Go,				"go" },
	{ Textures::LampPost,		"lamppost" },
	{ Textures::Tree,			"tree" },
	{ Textures::Destruction,	"destruction" },
	{ Textures::Bench,			"bench" },
	{ Textures::Collision,		"collision" },
 	{ Textures::Van,			"van" },
};

ImageClient::ImageClient(Textures whichTexture)
{
	isCollidable = false;
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture(ImageClient::textureMap_[whichTexture]));
	SetCollisionRadius(0);
}