#include <RoboCatClientPCH.h>


std::unique_ptr< SoundManager >		SoundManager::sInstance;

void SoundManager::StaticInit()
{
	sInstance.reset( new SoundManager() );
}

//IMPORTANT: TEXTURES
SoundManager::SoundManager()
{


	/*CacheTexture("justice", "../Assets/Justice.png");
	CacheTexture( "mouse", "../Assets/mouse.png" );
	CacheTexture( "yarn", "../Assets/yarn.png" );
	CacheTexture("background", "../Assets/background.png");
	CacheTexture("explosion", "../Assets/explosion.png");
	CacheTexture("justicepop", "../Assets/PopText.png");*/

	if (SDL_INIT_AUDIO < 0) {
		printf("SDL AUDIO COULDNT INITIALISE");
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0) {
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
	}

	mMusic = Mix_LoadMUS("../Assets/Audio/MissionTheme.wav");
	CacheSound(Sounds::Explosion1, "../Assets/Audio/Explosion1.wav");
	CacheSound(Sounds::TimeToKickAss, "../Assets/Audio/VoiceChewGum.wav");
	CacheSound(Sounds::DestroyTree, "../Assets/Audio/DestroyTree.wav");
	CacheSound(Sounds::DestroyBench, "../Assets/Audio/DestroyBench.wav");
	CacheSound(Sounds::DestroyLamp, "../Assets/Audio/DestroyLamp.wav");

}
//
//TexturePtr	TextureManager::GetTexture( const string& inTextureName )
//{
//	return mNameToTextureMap[ inTextureName ];
//}
//
bool SoundManager::CacheSound(Sounds sound, const char* inFileName)
{


	Mix_Chunk* audio = Mix_LoadWAV(inFileName);
	mNameToSoundMap[sound] = audio;

	return true;
}

void SoundManager::PlaySound(Sounds sound) {
	Mix_PlayChannel(-1, mNameToSoundMap[sound], 0);
}

void SoundManager::PlayMusic() {
	Mix_PlayMusic(mMusic, -1);
}