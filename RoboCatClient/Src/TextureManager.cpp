#include <RoboCatClientPCH.h>
#include <SDL_image.h>

std::unique_ptr< TextureManager >		TextureManager::sInstance;

void TextureManager::StaticInit()
{
	sInstance.reset( new TextureManager() );
}

//IMPORTANT: TEXTURES
TextureManager::TextureManager()
{
	CacheTexture( "robber",			"../Assets/Robber.png" );
	CacheTexture("justice",			"../Assets/Justice.png");
	CacheTexture( "mouse",			"../Assets/mouse.png" );
	CacheTexture( "yarn",			"../Assets/yarn.png" );
	CacheTexture("background",		"../Assets/background.png");
	CacheTexture("explosion",		"../Assets/explosion.png");
	CacheTexture("justicepop",		"../Assets/PopText.png");
	CacheTexture("van",				"../Assets/van.png");
	CacheTexture("skidmarkleft",	"../Assets/skidmarkleft.png");
	CacheTexture("skidmarkright",	"../Assets/skidmarkright.png");
	CacheTexture("lamppost",		"../Assets/lamppost.png");
	CacheTexture("tree",			"../Assets/tree.png");
	CacheTexture("bench",			"../Assets/bench.png");
	CacheTexture("destruction",     "../Assets/destruction.png");
	CacheTexture("go",				"../Assets/go.png");
	CacheTexture("ready",			"../Assets/ready.png");
	CacheTexture("collision",		"../Assets/collision.png");
	CacheTexture("suddendeath",		"../Assets/suddendeath.png");
}

TexturePtr	TextureManager::GetTexture( const string& inTextureName )
{
	return mNameToTextureMap[ inTextureName ];
}

bool TextureManager::CacheTexture( string inTextureName, const char* inFileName )
{
	SDL_Texture* texture = IMG_LoadTexture( GraphicsDriver::sInstance->GetRenderer(), inFileName );

	if( texture == nullptr )
	{
		SDL_LogError( SDL_LOG_CATEGORY_ERROR, "Failed to load texture: %s", inFileName );
		return false;
	}

	int w, h;
	SDL_QueryTexture( texture, nullptr, nullptr, &w, &h );

	// Set the blend mode up so we can apply our colors
	SDL_SetTextureBlendMode( texture, SDL_BLENDMODE_BLEND );
	
	TexturePtr newTexture( new Texture( w, h, texture ) );

	mNameToTextureMap[ inTextureName ] = newTexture;

	return true;

}