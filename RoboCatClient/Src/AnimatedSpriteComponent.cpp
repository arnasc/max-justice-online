#include <RoboCatClientPCH.h>

AnimatedSpriteComponent::AnimatedSpriteComponent( GameObject *inGameObject, int numFramesX, int numFramesY ) : SpriteComponent(inGameObject)
{
	AnimatedSpriteComponent::numFramesX = numFramesX;
	AnimatedSpriteComponent::numFramesY = numFramesY;
}

AnimatedSpriteComponent::~AnimatedSpriteComponent()
{	
	RenderManager::sInstance->RemoveComponent( this );
}

void AnimatedSpriteComponent::SetTexture(TexturePtr inTexture) {
	mTexture = inTexture;
	
	SDL_QueryTexture(mTexture->GetData(), NULL, NULL, &textureWidth, &textureHeight);

	frameWidth = textureWidth / numFramesX;
	frameHeight = textureHeight / numFramesY;

	sourceRect.x = 0;
	sourceRect.y = 0;
	sourceRect.h = frameHeight;
	sourceRect.w = frameWidth;
}


void AnimatedSpriteComponent::NextFrame() {

	sourceRect.x += frameWidth;

	if (sourceRect.x + frameWidth > textureWidth) {
		sourceRect.x = 0;
		sourceRect.y += frameWidth;

		if (sourceRect.y + frameHeight > textureHeight) {
			sourceRect.y = 0;
			finished = true;
			RenderManager::sInstance->RemoveComponent(this);
		}
	}
}

void AnimatedSpriteComponent::Draw( const SDL_Rect& inViewTransform )
{
	if( mTexture )
	{
		// Texture color multiplier
		Vector3 color = mGameObject->GetColor();
		Uint8 r = static_cast<Uint8>( color.mX * 255 );
		Uint8 g = static_cast<Uint8>( color.mY * 255 );
		Uint8 b = static_cast<Uint8>( color.mZ * 255 );
		SDL_SetTextureColorMod( mTexture->GetData(), r, g, b );

		// Compute the source rectangle
		
		//sourceRect.w = frameWidth;
		//sourceRect.h = frameHeight;




		//srcRect.x = frameWidth * (textureWidth/frameWidth); // %  currentFrame

		// Compute the destination rectangle
		Vector3 objLocation = mGameObject->GetLocation();
		float objScale = mGameObject->GetScale();
		SDL_Rect dstRect;
		dstRect.w = static_cast< int >( mTexture->GetWidth() * objScale );
		dstRect.h = static_cast< int >( mTexture->GetHeight() * objScale );
		dstRect.x = static_cast<int>( objLocation.mX * inViewTransform.w + inViewTransform.x - dstRect.w / 2 );
		dstRect.y = static_cast<int>( objLocation.mY * inViewTransform.h + inViewTransform.y - dstRect.h / 2 );

		dstRect.x += offset.mX;
		dstRect.y += offset.mY;

		float rotation = 0;
		if (doesRotationFollowPlayer) {
			rotation = RoboMath::ToDegrees(mGameObject->GetRotation());
		}
		
		// Blit the texture
		SDL_RenderCopyEx( GraphicsDriver::sInstance->GetRenderer(), mTexture->GetData(), &sourceRect,
			&dstRect, rotation, nullptr, SDL_FLIP_NONE );
	}
}