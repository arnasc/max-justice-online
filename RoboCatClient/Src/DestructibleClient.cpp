#include <RoboCatClientPCH.h>

DestructibleClient::DestructibleClient(Destructible::DestructibleType dType, ImageClient::Textures tType) : Destructible(Destructible::id)
{
	//if (dType == DestructibleType::DT_LampPost) {
	//	SetCollisionRadius(Destructible::LAMPPOST_SIZE);
	//}else if (dType == DestructibleType::DT_Tree) {
	//	SetCollisionRadius(Destructible::TREE_SIZE);
	//}
	Destructible::dType = dType;
	//mSpriteComponent.reset( new SpriteComponent( this ) );
	//mSpriteComponent->SetTexture( TextureManager::sInstance->GetTexture(ImageClient::textureMap_[tType]) );
}

void DestructibleClient::SetSprite(Destructible::DestructibleType dType) {
	ImageClient::Textures tType;
	if (dType == DestructibleType::DT_LampPost) {
		tType = ImageClient::Textures::LampPost;
	}
	else if (dType == DestructibleType::DT_Tree) {
		tType = ImageClient::Textures::Tree;
	}
	else if (dType == DestructibleType::DT_Bench) {
		tType = ImageClient::Textures::Bench;
	}

	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture(ImageClient::textureMap_[tType]));
}
//bool DestructibleClient::HandleCollisionWithCat(RoboCat* inCat)
//{
//	(void)inCat;
//	return false;
//}
//
//bool DestructibleClient::HandleCollisionWithJustice(Justice* inJustice)
//{
//	(void)inJustice;
//	return false;
//}


void DestructibleClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		Vector3 location;
		inInputStream.Read(location.mX);
		inInputStream.Read(location.mY);
		SetLocation(location);

		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		int type;
		inInputStream.Read(type);
		dType = static_cast<DestructibleType>(type);
		SetSprite(dType);
	}
}