class AnimatedSpriteComponent : public SpriteComponent
{
public:

	AnimatedSpriteComponent( GameObject* inGameObject, int numFramesX, int numFramesY);
	virtual	void		SetTexture(TexturePtr inTexture);

	~AnimatedSpriteComponent();

	int frameHeight, frameWidth;
	int textureHeight, textureWidth;
	int numFramesX, numFramesY, currentFrame = 0;
	bool finished = false;

	SDL_Rect sourceRect;


	virtual void		Draw( const SDL_Rect& inViewTransform );
	void NextFrame();
};

typedef shared_ptr< AnimatedSpriteComponent >	AnimatedSpriteComponentPtr;