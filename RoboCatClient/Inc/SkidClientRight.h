#include <map>
class SkidClientRight : public SkidRight, public ImageClient
{
public:
	CLASS_IDENTIFICATION('SKDR', GameObject)
		static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new SkidClientRight(Textures::SkidMarkRight)); }

	SkidClientRight(ImageClient::Textures whichTexture) : ImageClient(whichTexture) {};

protected:

	SpriteComponentPtr	mSpriteComponent;

private:


};

