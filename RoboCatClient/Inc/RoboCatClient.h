class RoboCatClient : public RoboCat
{
public:
	static	GameObjectPtr	StaticCreate()			{ return GameObjectPtr( new RoboCatClient() ); }
	static	GameObjectPtr	StaticCreateJustice()	{ return GameObjectPtr( new RoboCatClient(true) ); }

	virtual void	Update();
	virtual void	HandleDying() override;

	virtual void	Read( InputMemoryBitStream& inInputStream ) override;

	void DoClientSidePredictionAfterReplicationForLocalCat( uint32_t inReadState );
	void DoClientSidePredictionAfterReplicationForRemoteCat( uint32_t inReadState );
	void MoveCamera();

	SpriteComponentPtr mReadySprite;
	SpriteComponentPtr mSuddenDeathSprite;
	GameObjectPtr mGoSprite;
protected:
	RoboCatClient();
	RoboCatClient(bool isJustice);

private:

	bool mSuddenDeath = false;
	bool hasGoAnimPlayed = false;
	void InterpolateClientSidePrediction( float inOldRotation, const Vector3& inOldLocation, const Vector3& inOldVelocity, bool inIsForRemoteCat );

	float				mTimeLocationBecameOutOfSync;
	float				mTimeVelocityBecameOutOfSync;
	
	SpriteComponentPtr	mSpriteComponent;
};