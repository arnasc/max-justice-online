class JusticeClient : public Justice
{
public:
	static	GameObjectPtr	StaticCreate()			{ return GameObjectPtr( new JusticeClient() ); }

	virtual void	Update();
	virtual void	HandleDying() override;

	virtual void	Read( InputMemoryBitStream& inInputStream ) override;

	void DoClientSidePredictionAfterReplicationForLocalCat( uint32_t inReadState );
	void DoClientSidePredictionAfterReplicationForRemoteCat( uint32_t inReadState );
	void SetTeam(int newTeam);
	void MoveCamera();

protected:
	JusticeClient();
	JusticeClient(bool isJustice);

private:
	bool mSuddenDeath;
	void InterpolateClientSidePrediction( float inOldRotation, const Vector3& inOldLocation, const Vector3& inOldVelocity, bool inIsForRemoteCat );

	float				mTimeLocationBecameOutOfSync;
	float				mTimeVelocityBecameOutOfSync;
	
	SpriteComponentPtr	mSpriteComponent;
	SpriteComponentPtr	mSuddenDeathSprite;
};