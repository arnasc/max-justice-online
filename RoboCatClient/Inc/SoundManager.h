class SoundManager
{
public:
	enum Sounds {
		Explosion1,
		TimeToKickAss,
		DestroyTree,
		DestroyBench,
		DestroyLamp,
	};

	static void StaticInit();

	static std::unique_ptr< SoundManager >		sInstance;

	//TexturePtr	GetTexture( const string& inTextureName );
	void PlaySound(Sounds sound);
	void PlayMusic();

private:
	SoundManager();

	bool CacheSound( Sounds, const char* inFileName);


	unordered_map< Sounds, Mix_Chunk* >	mNameToSoundMap;
	Mix_Music* mMusic;
};