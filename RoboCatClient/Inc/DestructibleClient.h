class DestructibleClient : public Destructible
{
public:
	//static	GameObjectPtr	StaticCreateLampPost()		{ return GameObjectPtr( new DestructibleClient(Destructible::DT_LampPost, ImageClient::LampPost) ); }
	//static	GameObjectPtr	StaticCreateTree() { return GameObjectPtr(new DestructibleClient(Destructible::DT_Tree, ImageClient::Tree)); }

	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new DestructibleClient(Destructible::DT_Tree, ImageClient::Tree)); }

	virtual void	Read(InputMemoryBitStream& inInputStream) override;

	void SetSprite(Destructible::DestructibleType);


	//virtual bool HandleCollisionWithCat(RoboCat* inCat) override;
	//virtual bool HandleCollisionWithJustice(Justice* inJustice) override;

protected:
	DestructibleClient(Destructible::DestructibleType dType, ImageClient::Textures tType);

private:

	SpriteComponentPtr	mSpriteComponent;
};