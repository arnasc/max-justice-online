class AnimatedImageClient : public GameObject
{
public:

	static	GameObjectPtr	StaticCreate(ImageClient::Textures whichTexture, int numFramesX, int numFramesY) { return GameObjectPtr(new AnimatedImageClient(whichTexture, numFramesX, numFramesY)); }

	static	GameObjectPtr	StaticCreateExplosion() { return GameObjectPtr(new AnimatedImageClient(ImageClient::Textures::Explosion, 4, 4)); }
	static	GameObjectPtr	StaticCreateJusticePop() { return GameObjectPtr(new AnimatedImageClient(ImageClient::Textures::JusticePop, 4, 3)); }
	static	GameObjectPtr	StaticCreateDestruction() { return GameObjectPtr(new AnimatedImageClient(ImageClient::Textures::Destruction, 4, 4)); }
	static	GameObjectPtr	StaticCreateGO() { return GameObjectPtr(new AnimatedImageClient(ImageClient::Textures::Go, 8, 7, .7F)); }

	~AnimatedImageClient();

	virtual void	Update();

protected:
	AnimatedImageClient(ImageClient::Textures whichTexture, int numFramesX, int numFramesY);
	AnimatedImageClient(ImageClient::Textures whichTexture, int numFramesX, int numFramesY, float duration);
	AnimatedSpriteComponentPtr	mSpriteComponent;
	int   mNumOfFrames;
	float mDuration;
	float mTimeSinceLastFrame;
	float mTimePerFrame;

private:


};

