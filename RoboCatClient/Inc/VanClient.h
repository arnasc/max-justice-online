class VanClient : public Van
{
public:
	static	GameObjectPtr	StaticCreate()			{ return GameObjectPtr( new VanClient() ); }


protected:
	VanClient();

	virtual void		Read(InputMemoryBitStream& inInputStream) override;

private:

	SpriteComponentPtr	mSpriteComponent;
};