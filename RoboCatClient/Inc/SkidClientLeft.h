#include <map>
class SkidClientLeft : public SkidLeft, public ImageClient
{
public:
	CLASS_IDENTIFICATION('SKDL', GameObject)
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new SkidClientLeft(Textures::SkidMarkLeft)); }


protected:
	SkidClientLeft(Textures whichTexture) : ImageClient(whichTexture){};
	SpriteComponentPtr	mSpriteComponent;

private:


};

