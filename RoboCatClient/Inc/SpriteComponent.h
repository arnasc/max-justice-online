class SpriteComponent
{
public:

	SpriteComponent( GameObject* inGameObject );
	~SpriteComponent();

	virtual void		Draw( const SDL_Rect& inViewTransform );

	virtual	void		SetTexture( TexturePtr inTexture )			{ mTexture = inTexture; }

			Vector3		GetOrigin()					const			{ return mOrigin; }
			void		SetOrigin( const Vector3& inOrigin )		{ mOrigin = inOrigin; }
			TexturePtr										mTexture;

			bool doesRotationFollowPlayer = true;
			Vector3 offset = Vector3::Zero;

protected:

	Vector3											mOrigin;

	

	//don't want circular reference...
	GameObject*										mGameObject;
};

typedef shared_ptr< SpriteComponent >	SpriteComponentPtr;