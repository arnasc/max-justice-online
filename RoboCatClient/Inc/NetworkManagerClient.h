


class NetworkManagerClient : public NetworkManager
{
	// IMPORTANT: CLIENT STATES
	enum NetworkClientState
	{
		NCS_Uninitialized,
		NCS_SayingHello,
		NCS_Welcomed,
	};

public:
	static NetworkManagerClient*	sInstance;

	static	void	StaticInit( const SocketAddress& inServerAddress, const string& inName, const int inTeam );

			void	SendOutgoingPackets();

	virtual void	ProcessPacket( InputMemoryBitStream& inInputStream, const SocketAddress& inFromAddress ) override;

			const	WeightedTimedMovingAverage&		GetAvgRoundTripTime()	const	{ return mAvgRoundTripTime; }
			float									GetRoundTripTime()		const	{ return mAvgRoundTripTime.GetValue(); }
			int		GetPlayerId()											const	{ return mPlayerId; }
			float	GetLastMoveProcessedByServerTimestamp()					const	{ return mLastMoveProcessedByServerTimestamp; }
			int					mTeam;
			bool	mPlayerReady = false;
			bool	mGameStarted = false;
			bool	mSuddenDeath = false;

private:
			NetworkManagerClient();
			void Init( const SocketAddress& inServerAddress, const string& inName , const int inTeam);

			void	UpdateSayingHello();
			void	SendHelloPacket();

			void	UpdateSayingReady();
			void	SendReadyPacket();

			void	HandleWelcomePacket( InputMemoryBitStream& inInputStream );
			void	HandleStartGamePacket(InputMemoryBitStream& inInputStream);
			void	HandleStatePacket( InputMemoryBitStream& inInputStream );
			void    HandleSpriteCreationPacket(InputMemoryBitStream& inInputStream);
			void	HandleSuddenDeathPacket(InputMemoryBitStream& inInputStream);
			void	ReadLastMoveProcessedOnServerTimestamp( InputMemoryBitStream& inInputStream );
			void	HandleSoundPacket(InputMemoryBitStream& inInputStream);

			void	HandleGameObjectState( InputMemoryBitStream& inInputStream );
			void	HandleScoreBoardState( InputMemoryBitStream& inInputStream );

			void	UpdateSendingInputPacket();
			void	SendInputPacket();

			void	DestroyGameObjectsInMap( const IntToGameObjectMap& inObjectsToDestroy );


	
	DeliveryNotificationManager	mDeliveryNotificationManager;
	ReplicationManagerClient	mReplicationManagerClient;

	SocketAddress		mServerAddress;

	NetworkClientState	mState;

	float				mTimeOfLastHello;
	float				mTimeOfLastReady;
	float				mTimeOfLastInputPacket;

	string				mName;
	int					mPlayerId;

				


	float				mLastMoveProcessedByServerTimestamp;

	WeightedTimedMovingAverage	mAvgRoundTripTime;
	float						mLastRoundTripTime;

};