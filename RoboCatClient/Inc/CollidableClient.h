class CollidableClient : public Collidable
{
public:
	static	GameObjectPtr	StaticCreate()		{ return GameObjectPtr( new CollidableClient() ); }

protected:
	CollidableClient();

private:
};