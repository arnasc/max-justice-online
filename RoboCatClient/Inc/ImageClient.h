#include <map>
class ImageClient : public virtual GameObject
{
public:


	enum Textures {
		Map,
		Explosion,
		JusticePop,
		SkidMarkLeft,
		SkidMarkRight,
		LampPost,
		Tree,
		Destruction,
		Bench,
		Go,
		Collision,
		Van,
	};

	typedef std::map<Textures, std::string> TextureMap;
	static TextureMap textureMap_;


	static	GameObjectPtr	StaticCreate(Textures whichTexture) { return GameObjectPtr(new ImageClient(whichTexture)); }
	static	GameObjectPtr	StaticCreateVan() { return GameObjectPtr(new ImageClient(Van)); }

protected:
	ImageClient(Textures whichTexture);
	SpriteComponentPtr	mSpriteComponent;

private:


};

