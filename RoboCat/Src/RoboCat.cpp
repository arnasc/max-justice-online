#include <RoboCatPCH.h>

//IMPORTANT: LOCAL VERSION OF CAT

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 14.0f;
const float HALF_WORLD_WIDTH = 10.5f;

// IMPORTANT: SPEEDS OF CAT
RoboCat::RoboCat() :
	GameObject(),
	mMaxRotationSpeed( 4.f ),
	mMaxLinearSpeed( 8.f ),
	mVelocity( Vector3::Zero ),
	mWallRestitution( 0.1f ),
	mCatRestitution( 0.1f ),
	mThrustDir( 0.f ),
	mPlayerId( 0 ),
	mIsShooting( false ),
	mHealth( 10 )
{
	SetCollisionRadius( 0.25f );
}

void RoboCat::ProcessInput( float inDeltaTime, const InputState& inInputState )
{
	//process our input....

	//turning...
	float newRotation = GetRotation() + inInputState.GetDesiredHorizontalDelta() * mMaxRotationSpeed * inDeltaTime;
	
	isSkiddingLeft = false;
	isSkiddingRight = false;
	//if (mVelocity.Length() > 3 && mVelocity.Length() < 7) {
	//	if (newRotation - GetRotation() > .1F) {
	//		isSkiddingLeft = true;
	//	}
	//	else if (GetRotation() - newRotation > .1F) {
	//		isSkiddingRight = true;
	//	}
	//	
	//}


	SetRotation( newRotation );

	//moving...
	float inputForwardDelta = inInputState.GetDesiredVerticalDelta();
	mThrustDir = inputForwardDelta;


	mIsShooting = inInputState.IsShooting(); 
}

void RoboCat::AdjustVelocityByThrust( float inDeltaTime )
{
	// DECELLERATION
	//Vector3 velocityDecelleration = Vector3(-4, -4, 0);
	//if (mVelocity.mX < 0) {
	//	velocityDecelleration.mX *= -1;
	//}

	//if (mVelocity.mY < 0) {
	//	velocityDecelleration.mY *= -1;
	//}

	//if (mVelocity.Length() > 0) {
	//	mVelocity += velocityDecelleration * inDeltaTime;
	//}

	//if (mVelocity.Length() > 0 && mVelocity.Length() < .1F) {
	//	mVelocity = Vector3::Zero;
	//}

	//// ACCELERATION
	//Vector3 forwardVector = GetForwardVector();
	//mVelocity += forwardVector * ( mThrustDir * inDeltaTime * mMaxLinearSpeed );

	//// DIRECTIONAL ACCELERATION
	//float radians = M_PI / 180;
	//float rot = GetRotation();
	//float radianAngle = (GetRotation()) - 1.5708f;

	//Vector3 unitDirectionOfVelocity = mVelocity.NormalizeReturn();
	//float velocityMagnitude = mVelocity.Length();

	//// not sure what this does
	//if (velocityMagnitude < .5f) {
	//	return;
	//}

	//Vector3 unitDirectionOfSprite = Vector3(std::cos(radianAngle), std::sin(radianAngle), 0);

	//// need to implement handling variable
	//Vector3 lerpDirection = Lerp(unitDirectionOfVelocity, unitDirectionOfSprite, 5.F * inDeltaTime);
	//Vector3 newVelocity = lerpDirection * velocityMagnitude;
	//mVelocity = newVelocity;

	Vector3 forwardVector = GetForwardVector();
	mVelocity = forwardVector * (mThrustDir * inDeltaTime * mMaxLinearSpeed);
}


void RoboCat::SimulateMovement( float inDeltaTime )
{
	//simulate us...
	AdjustVelocityByThrust( inDeltaTime );


	SetLocation( GetLocation() + mVelocity * inDeltaTime );

	ProcessCollisions();

	
}

void RoboCat::Update()
{
	
}


void RoboCat::HandleCollisionWithWall(SDL_FloatRect player, SDL_FloatRect wall) {
	bool leftCollision;
	bool rightCollision;
	bool topCollision;
	bool bottomCollision;


	float playerLeft = player.x;
	float playerRight = playerLeft + player.w;
	float playerTop = player.y;
	float playerBottom = playerTop + player.h;

	float wallLeft = wall.x;
	float wallRight = wallLeft + wall.w;
	float wallTop = wall.y;
	float wallBottom = wallTop + wall.h;

	leftCollision = playerLeft < wallLeft && playerRight >= wallLeft;
	rightCollision = playerRight > wallRight && playerLeft <= wallRight;
	topCollision = playerTop < wallTop && playerBottom >= wallTop;
	bottomCollision = playerBottom > wallBottom && playerTop <= wallBottom;


	float xVelocity = GetVelocity().mX;
	float yVelocity = GetVelocity().mY;

	if (leftCollision) {
		if (topCollision) {
			if (abs(yVelocity) > abs(xVelocity)) {
				// Keep travelling upwards
				SetVelocity(Vector3(0.f, yVelocity, 0));
			}
			//else {
			//	// Keep travelling left
			//	aircraft.setVelocity(xVelocity, 0.f);
			//}
		}
		else if (bottomCollision) {
			if (abs(yVelocity) > abs(xVelocity)) {
				// keep travelling down
				SetVelocity(Vector3(0.f, yVelocity,0));
			}
			//else {
			//	// keep travelling left
			//	aircraft.setVelocity(xVelocity, 0.f);
			//}
		}
		else {
			// Keep travelling up/down
			SetVelocity(Vector3(-1.F, yVelocity,0));
		}
	}
	else if (rightCollision) {
		if (topCollision) {
			if (abs(yVelocity) > abs(xVelocity)) {
				// Keep travelling upwards
				SetVelocity(Vector3(0.f, yVelocity, 0));
			}
			//else {
			//	// Keep travelling right
			//	aircraft.setVelocity(xVelocity, 0.f);
			//}
		}
		else if (bottomCollision) {
			if (abs(yVelocity) > abs(xVelocity)) {
				// keep travelling down
				SetVelocity(Vector3(0.f, yVelocity, 0));
			}
			//else {
			//	// keep travelling right
			//	aircraft.setVelocity(xVelocity, 0.f);
			//}
		}
		else {
			// Keep travelling up/down
			SetVelocity(Vector3(1.F, yVelocity, 0));
		}
	}
	else if (topCollision) {
		if (leftCollision) {
			if (abs(yVelocity) > abs(xVelocity)) {
				// Keep travelling left
				SetVelocity(Vector3(xVelocity, 0,0));
			}
			//else {
			//	// Keep travelling left
			//	aircraft.setVelocity(xVelocity, 0.f);
			//}
		}
		else if (rightCollision) {
			if (abs(yVelocity) > abs(xVelocity)) {
				// keep travelling right
				SetVelocity(Vector3(xVelocity, 0, 0));
			}
			//else {
			//	// keep travelling left
			//	aircraft.setVelocity(xVelocity, 0.f);
			//}
		}
		else {
			// Keep travelling left/right
			SetVelocity(Vector3(xVelocity, -1.F,0));
		}
	}
	else if (bottomCollision) {
		if (leftCollision) {
			if (abs(yVelocity) > abs(xVelocity)) {
				// Keep travelling left
				SetVelocity(Vector3(xVelocity, 0, 0));
			}
			//else {
			//	// Keep travelling left
			//	aircraft.setVelocity(xVelocity, 0.f);
			//}
		}
		else if (rightCollision) {
			if (abs(yVelocity) > abs(xVelocity)) {
				// keep travelling right
				SetVelocity(Vector3(xVelocity, 0, 0));
			}
			//else {
			//	// keep travelling left
			//	aircraft.setVelocity(xVelocity, 0.f);
			//}
		}
		else {
			// Keep travelling left/right
			SetVelocity(Vector3(xVelocity, 1.F, 0));
		}
	}
}

// IMPORTANT: COLLIOSIONS
void RoboCat::ProcessCollisions()
{
	//right now just bounce off the sides..
	ProcessCollisionsWithScreenWalls();

	float sourceRadius = GetCollisionRadius();
	Vector3 sourceLocation = GetLocation();

	//now let's iterate through the world and see what we hit...
	//note: since there's a small number of objects in our game, this is fine.
	//but in a real game, brute-force checking collisions against every other object is not efficient.
	//it would be preferable to use a quad tree or some other structure to minimize the
	//number of collisions that need to be tested.
	for( auto goIt = World::sInstance->GetGameObjects().begin(), end = World::sInstance->GetGameObjects().end(); goIt != end; ++goIt )
	{
		GameObject* target = goIt->get();
		if( target != this && !target->DoesWantToDie() )
		{
			if (!target->isCollidable) {
				return;
			}

			if (target->isCollisionWall) {
				Vector3 pos = GetLocation();
				Vector3 targetPos = target->GetLocation();
				float radius = GetCollisionRadius();
				//SDL_CollideBoundingBox

				SDL_FloatRect player;
				SDL_FloatRect wall;

				player.x = pos.mX -= radius;
				player.y = pos.mY -= radius;
				player.w = radius * 2;
				player.h = radius * 2;

				wall.x = targetPos.mX -= (target->GetWidth() * .5F);
				wall.y = targetPos.mY -= (target->GetHeight() * .5F);
				wall.w = target->GetWidth();
				wall.h = target->GetHeight();

				if (SDL_CollideBoundingBox(player,wall)) {
					HandleCollisionWithWall(player,wall);
					return;
				}
				//if (SDL_CollideBoundingBoxWithCircle(GetCollisionRadius(), pos.mX, pos.mY, target->GetWidth(), target->GetHeight(), targetPos.mX, targetPos.mY)) {
				//	SetVelocity(Vector3(0, 0, 0));//HandleCollisionWithWall(target);
				//}
			}

			//simple collision test for spheres- are the radii summed less than the distance?
			Vector3 targetLocation = target->GetLocation();
			float targetRadius = target->GetCollisionRadius();

			Vector3 delta = targetLocation - sourceLocation;
			float distSq = delta.LengthSq2D();
			float collisionDist = ( sourceRadius + targetRadius );
			if( distSq < ( collisionDist * collisionDist ) )
			{
				//first, tell the other guy there was a collision with a cat, so it can do something...



				if( target->HandleCollisionWithCat( this ) )
				{
					//okay, you hit something!
					//so, project your location far enough that you're not colliding
					Vector3 dirToTarget = delta;
					dirToTarget.Normalize2D();
					Vector3 acceptableDeltaFromSourceToTarget = dirToTarget * collisionDist;
					//important note- we only move this cat. the other cat can take care of moving itself
					SetLocation( targetLocation - acceptableDeltaFromSourceToTarget );

					
					Vector3 relVel = mVelocity;
				
					//if other object is a cat, it might have velocity, so there might be relative velocity...
					RoboCat* targetCat = target->GetAsCat();
					if( targetCat )
					{
						relVel -= targetCat->mVelocity;
					}

					//got vel with dir between objects to figure out if they're moving towards each other
					//and if so, the magnitude of the impulse ( since they're both just balls )
					float relVelDotDir = Dot2D( relVel, dirToTarget );

					if (relVelDotDir > 0.f)
					{
						Vector3 impulse = relVelDotDir * dirToTarget;
					
						if( targetCat )
						{
							mVelocity -= impulse;
							mVelocity *= mCatRestitution;
						}
						else
						{
							mVelocity -= impulse * 2.f;
							mVelocity *= mWallRestitution;
						}

					}
				}
			}
		}
	}

}

// IMPORTANT: COLLISION
void RoboCat::ProcessCollisionsWithScreenWalls()
{
	Vector3 location = GetLocation();
	float x = location.mX;
	float y = location.mY;

	float vx = mVelocity.mX;
	float vy = mVelocity.mY;

	float radius = GetCollisionRadius();

	//if the cat collides against a wall, the quick solution is to push it off
	if( ( y + radius ) >= HALF_WORLD_HEIGHT && vy > 0 )
	{
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}
	else if( y <= ( -HALF_WORLD_HEIGHT - radius ) && vy < 0 )
	{
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = -HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}

	if( ( x + radius ) >= HALF_WORLD_WIDTH && vx > 0 )
	{
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}
	else if(  x <= ( -HALF_WORLD_WIDTH - radius ) && vx < 0 )
	{
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = -HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}
}

uint32_t RoboCat::Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const
{
	uint32_t writtenState = 0;

	if( inDirtyState & ECRS_PlayerId )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetPlayerId() );

		writtenState |= ECRS_PlayerId;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}


	if( inDirtyState & ECRS_Pose )
	{
		inOutputStream.Write( (bool)true );

		Vector3 velocity = mVelocity;
		inOutputStream.Write( velocity.mX );
		inOutputStream.Write( velocity.mY );

		Vector3 location = GetLocation();
		inOutputStream.Write( location.mX );
		inOutputStream.Write( location.mY );

		inOutputStream.Write( GetRotation() );

		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	//always write mThrustDir- it's just two bits
	if( mThrustDir != 0.f )
	{
		inOutputStream.Write( true );
		inOutputStream.Write( mThrustDir > 0.f );
	}
	else
	{
		inOutputStream.Write( false );
	}

	if( inDirtyState & ECRS_Color )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetColor() );

		writtenState |= ECRS_Color;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	if( inDirtyState & ECRS_Health )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( mHealth, 4 );

		writtenState |= ECRS_Health;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	/*if (inDirtyState & ECRS_Team)
	{
		inOutputStream.Write((bool)true);
		int iTeam = team;
		inOutputStream.Write(iTeam, 4);

		writtenState |= ECRS_Team;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}*/

	return writtenState;
	

}


