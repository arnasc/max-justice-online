#include <RoboCatPCH.h>
int Destructible::id = 0;

//IMPORTANT: LOCAL VERSION OF Destructible
Destructible::Destructible(int id)
{
	SetScale( GetScale() * 1.f );
	SetCollisionRadius( 0.25f );
	Destructible::id++;
	mID = id;
	dType = DestructibleType::DT_Tree;
}

bool Destructible::HandleCollisionWithCat( RoboCat* inCat )
{
	( void ) inCat;
	return false;
}

bool Destructible::HandleCollisionWithJustice(Justice* inJustice)
{
	(void)inJustice;
	return false;
}


uint32_t Destructible::Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const
{
	uint32_t writtenState = 0;

	if( inDirtyState & EDRS_Pose)
	{
		inOutputStream.Write( (bool)true );

		Vector3 location = GetLocation();
		inOutputStream.Write( location.mX );
		inOutputStream.Write( location.mY );

		inOutputStream.Write( GetRotation() );

		writtenState |= EDRS_Pose;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	if (inDirtyState & EDRS_Type)
	{
		inOutputStream.Write((bool)true);
		int type = dType;
		inOutputStream.Write(type);

		writtenState |= EDRS_Type;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	return writtenState;
}

void Destructible::Read( InputMemoryBitStream& inInputStream )
{
	bool stateBit;

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		Vector3 location;
		inInputStream.Read(location.mX);
		inInputStream.Read(location.mY);
		SetLocation(location);

		float rotation;
		inInputStream.Read(rotation);
		SetRotation(rotation);
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		int type;
		inInputStream.Read(type);
		dType = static_cast<DestructibleType>(type);
	}
}

