class Collidable : public GameObject
{
public:
	CLASS_IDENTIFICATION('COLL', GameObject)

		// IMPORTANT: MOUSE REPLICATION STATES
		enum ECollReplicationState
		{
			EMRS_Pose		= 1 << 0,

			EMRS_AllState	= EMRS_Pose
		};

	int width = 3;
	int height = 3;

	virtual int GetWidth() override { return width; };
	virtual int GetHeight() override { return height; };

	static	GameObject*	StaticCreate() { return new Collidable(); }

	virtual uint32_t	GetAllStateMask()	const override	{ return EMRS_AllState; }

	virtual const Vector3&	 GetLocation()	const override { return mLocation; }
	virtual uint32_t	Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const override;
	virtual void		Read( InputMemoryBitStream& inInputStream ) override;

	virtual bool HandleCollisionWithCat( RoboCat* inCat ) override;

protected:
	Collidable();

};