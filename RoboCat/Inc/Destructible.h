class Destructible : public virtual GameObject
{
public:
	CLASS_IDENTIFICATION( 'DSTR', GameObject)

	// IMPORTANT: DESTRUCTIBLE REPLICATION STATES
	enum DestructibleType
	{
		DT_LampPost = 0,
		DT_Bench = 1,
		DT_Tree = 2,
		DT_Bush = 3,
		DT_Bin = 4,
	};

	enum EDestructibleReplicationState
	{
		EDRS_Pose = 1 << 0,
		EDRS_Type = 1 << 1,

		EDRS_AllState = EDRS_Pose | EDRS_Type
	};

	virtual	Destructible*	GetAsDestructible() override { return this; }
	virtual	void SetDestructibleType(int type) override { dType = static_cast<DestructibleType>(type); }

	const float LAMPPOST_SIZE = 3.F;
	const float BENCH_SIZE = 3.F;
	const float TREE_SIZE = 3.F;
	const float BUSH_SIZE = 2.F;
	const float BIN_SIZE = 2.F;
	static int id;
	int mID;
	DestructibleType dType;


	static	GameObject*	StaticCreate() {return new Destructible(Destructible::id); }

	virtual uint32_t	GetAllStateMask()	const override { return EDRS_AllState; }

	virtual uint32_t	Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const override;
	virtual void		Read( InputMemoryBitStream& inInputStream ) override;

	virtual bool HandleCollisionWithCat( RoboCat* inCat ) override;
	virtual bool HandleCollisionWithJustice(Justice* inJustice) override;
protected:
	Destructible(int ID);

};