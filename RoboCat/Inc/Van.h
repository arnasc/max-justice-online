class Van : public GameObject
{
public:
	CLASS_IDENTIFICATION( 'AVAN', GameObject )

	enum EVanReplicationState
	{
		EMRS_Pose = 1 << 0,

		EMRS_AllState = EMRS_Pose
	};

	virtual	Van*	GetAsVan() override { return this; }

	static	GameObject*	StaticCreate()			{ return new Van(); }

	virtual uint32_t	GetAllStateMask()	const override { return EMRS_AllState; }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	virtual void		Read(InputMemoryBitStream& inInputStream) override;

	virtual bool HandleCollisionWithCat(RoboCat* inCat) override;


protected:
	Van();

};

typedef shared_ptr< Van >	VanPtr;