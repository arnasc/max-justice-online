class Skid: public virtual GameObject
{
public:
	CLASS_IDENTIFICATION('SKDL', GameObject)

	static	GameObject*	StaticCreate() { return new Skid(); }

	enum SkidReplicationState
	{
		EMRS_Pose = 1 << 0,

		EMRS_AllState = EMRS_Pose
	};

	virtual uint32_t	GetAllStateMask()	const override { return EMRS_AllState; }

	virtual uint32_t	Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;
	virtual void		Read(InputMemoryBitStream& inInputStream) override;

protected:
	Skid();

private:


};

