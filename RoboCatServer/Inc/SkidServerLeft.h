class SkidServerLeft : public SkidLeft
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new SkidServerLeft() ); }
	void HandleDying() override;



	virtual bool		HandleCollisionWithCat( RoboCat* inCat ) override;

protected:
	SkidServerLeft();

};