class DestructibleServer : public Destructible
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new DestructibleServer() ); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithCat( RoboCat* inCat ) override;
	virtual bool		HandleCollisionWithJustice(Justice* inCat) override;

protected:
	DestructibleServer();

};