class Server : public Engine
{
public:

	static bool StaticInit();

	virtual void DoFrame() override;

	virtual int Run();

	void HandleNewClient( ClientProxyPtr inClientProxy );
	void HandleLostClient( ClientProxyPtr inClientProxy );


	JusticePtr FindJustice();
	VanPtr FindVan();
	RoboCatPtr	GetCatForPlayer( int inPlayerId );
	void	SpawnCatForPlayer( int inPlayerId );
	void	SpawnVan();
	void	TurnPlayerToJustice( int inPlayerId );
	void    SpawnExplosionSprite(int inPlayerId);

	bool gameOver = false;
private:
	Server();

	bool	InitNetworkManager();
	void	SetupWorld();
	//void	AddDestructibles();
};