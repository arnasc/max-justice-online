class VanServer : public Van
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new VanServer() ); }

	void HandleDying() override;

	virtual bool		HandleCollisionWithCat(RoboCat* inCat) override;


protected:
	VanServer();

private:

};