enum EJusticeControlType
{
	ESJT_Human,
	ESJT_AI
};

class JusticeServer : public Justice
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new JusticeServer() ); }
	virtual void HandleDying() override;

	virtual void Update();

	void SetCatControlType(EJusticeControlType inCatControlType ) { mCatControlType = inCatControlType; }

	virtual bool		HandleCollisionWithCat(RoboCat* inCat) override;

	void TakeDamage( int inDamagingPlayerId );
	void TurnToJustice(int inPlayerToJusticeId);

protected:
	JusticeServer();

private:

	void HandleShooting();

	EJusticeControlType	mCatControlType;
	float lastSkidMark;

	float		mTimeOfNextShot;
	float		mTimeBetweenShots;

};