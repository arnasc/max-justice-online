class NetworkManagerServer : public NetworkManager
{
public:
	static NetworkManagerServer*	sInstance;


	enum Textures {
		Map,
		Explosion,
		JusticePop,
		SkidMarkLeft,
		SkidMarkRight,
		LampPost,
		Tree,
		Destruction,
		Bench,
	};

	bool JusticeSpawned = false;

	static bool				StaticInit( uint16_t inPort );
		
	virtual void			ProcessPacket( InputMemoryBitStream& inInputStream, const SocketAddress& inFromAddress ) override;
	virtual void			HandleConnectionReset( const SocketAddress& inFromAddress ) override;
		
			void			SendOutgoingPackets();
			void			CheckForDisconnects();
			void			SendSpriteAllClients(Vector3 catLocation, Textures whichSprite);
			void			SendSoundToClient(int dType , int inPlayerId);
			void			SendSuddenDeathAllClients();

			void			RegisterGameObject( GameObjectPtr inGameObject );
	inline	GameObjectPtr	RegisterAndReturn( GameObject* inGameObject );
			void			UnregisterGameObject( GameObject* inGameObject );
			void			SetStateDirty( int inNetworkId, uint32_t inDirtyState );

			void			RespawnCats();

			ClientProxyPtr	GetClientProxy( int inPlayerId ) const;
			std::vector<bool> 		mPlayersReady;

			int timeOfJusticePacketSent = 0;

private:
			NetworkManagerServer();

			void	HandlePacketFromNewClient( InputMemoryBitStream& inInputStream, const SocketAddress& inFromAddress );
			void	ProcessPacket( ClientProxyPtr inClientProxy, InputMemoryBitStream& inInputStream );
			
			void	SendWelcomePacket( ClientProxyPtr inClientProxy );
			void	UpdateAllClients();
			
			void	StartGame();
			
			void	AddWorldStateToPacket( OutputMemoryBitStream& inOutputStream );
			void	AddScoreBoardStateToPacket( OutputMemoryBitStream& inOutputStream );

			void	SendStatePacketToClient( ClientProxyPtr inClientProxy );
			void SendSuddenDeathPacketToClient(ClientProxyPtr inClientProxy);

			void    SendSpritePacketToClient(ClientProxyPtr inClientProxy, Vector3 catLocation, Textures whichSprite);

			void	SendStartGamePacketToClient(ClientProxyPtr inClientProxy);

			void	WriteLastMoveTimestampIfDirty( OutputMemoryBitStream& inOutputStream, ClientProxyPtr inClientProxy );

			void	HandleInputPacket( ClientProxyPtr inClientProxy, InputMemoryBitStream& inInputStream );

			void	HandleReadyPacket(ClientProxyPtr inClientProxy, InputMemoryBitStream& inInputStream);
			void	HandleDeadPacket(ClientProxyPtr inClientProxy, InputMemoryBitStream& inInputStream);

			void	CheckAllPlayersReady();

			void	HandleClientDisconnected( ClientProxyPtr inClientProxy );

			int		GetNewNetworkId();

	typedef unordered_map< int, ClientProxyPtr >	IntToClientMap;
	typedef unordered_map< SocketAddress, ClientProxyPtr >	AddressToClientMap;

	AddressToClientMap		mAddressToClientMap;
	IntToClientMap			mPlayerIdToClientMap;


	int				mNewPlayerId;
	int				mNewNetworkId;

	bool gameStarted = false;

	float			mTimeOfLastSatePacket;
	float			mTimeBetweenStatePackets;
	float			mClientDisconnectTimeout;
};


inline GameObjectPtr NetworkManagerServer::RegisterAndReturn( GameObject* inGameObject )
{
	GameObjectPtr toRet( inGameObject );
	RegisterGameObject( toRet );
	return toRet;
}
