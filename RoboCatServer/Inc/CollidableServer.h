class CollidableServer : public Collidable
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new CollidableServer() ); }
	//void HandleDying() override;
	//virtual bool		HandleCollisionWithCat( RoboCat* inCat ) override;

protected:
	CollidableServer();

};