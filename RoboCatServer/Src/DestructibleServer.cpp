#include <RoboCatServerPCH.h>


DestructibleServer::DestructibleServer() : Destructible(Destructible::id)
{
}

void DestructibleServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

// IMPORTANT: MICE COLLIDES WITH CAT AND DIES
bool DestructibleServer::HandleCollisionWithCat( RoboCat* inCat )
{
	//kill yourself!
	SetDoesWantToDie( true );
	NetworkManagerServer::sInstance->SendSpriteAllClients(GetLocation(), NetworkManagerServer::Destruction);

	NetworkManagerServer::sInstance->SendSoundToClient(dType, inCat->GetPlayerId());

	//NetworkManagerServer::sInstance->SendSpriteAllClients(inCat->GetLocation(), NetworkManagerServer::JusticePop);
	//ScoreBoardManager::sInstance->IncScore( inCat->GetPlayerId(), 1 );

	return false;
}

// IMPORTANT: MICE COLLIDES WITH CAT AND DIES
bool DestructibleServer::HandleCollisionWithJustice(Justice* inCat)
{
	//kill yourself!
	SetDoesWantToDie(true);
	NetworkManagerServer::sInstance->SendSpriteAllClients(GetLocation(), NetworkManagerServer::Destruction);
	//NetworkManagerServer::sInstance->SendSpriteAllClients(inCat->GetLocation(), NetworkManagerServer::JusticePop);
	//ScoreBoardManager::sInstance->IncScore( inCat->GetPlayerId(), 1 );

	return false;
}



