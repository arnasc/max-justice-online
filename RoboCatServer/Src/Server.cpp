
#include <RoboCatServerPCH.h>



//uncomment this when you begin working on the server

bool Server::StaticInit()
{
	sInstance.reset( new Server() );

	return true;
}

Server::Server()
{
	// IMPORTANT: SERVER CODE FOR CREATING MICE, YARN AND BULLETS
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RCAT', RoboCatServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RJUS', JusticeServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'AVAN', VanServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'YARN', YarnServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'SKDR', SkidServerRight::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'SKDL', SkidServerLeft::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'DSTR', DestructibleServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'COLL', CollidableServer::StaticCreate);

	InitNetworkManager();
	
	//NetworkManagerServer::sInstance->SetDropPacketChance( 0.8f );
	NetworkManagerServer::sInstance->SetSimulatedLatency( 0.25f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.5f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.1f );

}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	string portString = StringUtils::GetCommandLineArg( 1 );
	uint16_t port = stoi( portString );

	return NetworkManagerServer::StaticInit( port );
}


namespace
{
	
	void CreateRandomMice( int inMouseCount )
	{
		Vector3 mouseMin( -5.f, -3.f, 0.f );
		Vector3 mouseMax( 5.f, 3.f, 0.f );
		GameObjectPtr go;

		//make a mouse somewhere- where will these come from?
		for( int i = 0; i < inMouseCount; ++i )
		{
			go = GameObjectRegistry::sInstance->CreateGameObject( 'MOUS' );
			Vector3 mouseLocation = RoboMath::GetRandomVector( mouseMin, mouseMax );
			go->SetLocation( mouseLocation );
		}
	}

	void CreateVan() {
		Vector3 mouseMin(-5.f, -3.f, 0.f);
		Vector3 mouseMax(5.f, 3.f, 0.f);

		Vector3 mouseLocation = RoboMath::GetRandomVector(mouseMin, mouseMax);

		GameObjectPtr go = GameObjectRegistry::sInstance->CreateGameObject('AVAN');
		go->SetLocation(Vector3(100,100,0));
		go->SetRotation(RoboMath::GetRandomFloat() * 360);


		
		

		//go1 = GameObjectRegistry::sInstance->CreateGameObject('TREE');
		//go1->SetLocation(Vector3(1, 5, 0));

		//go1 = GameObjectRegistry::sInstance->CreateGameObject('TREE');
		//go1->SetLocation(Vector3(1, 7, 0));
	}
	void CreateDestructible(int type, Vector3 location) {
		GameObjectPtr go1 = GameObjectRegistry::sInstance->CreateGameObject('DSTR');
		go1->SetDestructibleType(type);
		go1->SetLocation(location);
		NetworkManagerServer::sInstance->SetStateDirty(go1->GetNetworkId(), Destructible::EDestructibleReplicationState::EDRS_Type | Destructible::EDestructibleReplicationState::EDRS_Pose);
	}

	void CreateCollision(Vector3 location) {
		GameObjectPtr go1 = GameObjectRegistry::sInstance->CreateGameObject('COLL');
		go1->SetLocation(location);
		//NetworkManagerServer::sInstance->SetStateDirty(go1->GetNetworkId(), Destructible::EDestructibleReplicationState::EDRS_Pose);
	}

	void AddCollisions()
	{
	/*	CreateCollision(Vector3(-3.1, -6.3f, 0));
		CreateCollision(Vector3(-3.1, -5, 0));
		CreateCollision(Vector3(2.2, -6.3f, 0));
		CreateCollision(Vector3(2.2, -5, 0));

		CreateCollision(Vector3(-3.1, -10.5f, 0));
		CreateCollision(Vector3(2.2, -10.5f, 0));

		CreateCollision(Vector3(-8.4, -7, 0));
		CreateCollision(Vector3(-8.4, -5, 0));
		CreateCollision(Vector3(7.5, -7, 0));
		CreateCollision(Vector3(7.5, -5, 0));

		CreateCollision(Vector3(-3.1, 5.3f, 0));
		CreateCollision(Vector3(-3.1, 4, 0));
		CreateCollision(Vector3(2.2, 5.3f, 0));
		CreateCollision(Vector3(2.2, 4, 0));

		CreateCollision(Vector3(-3.1, 9.5f, 0));
		CreateCollision(Vector3(2.2, 9.5f, 0));
		
		CreateCollision(Vector3(-8.4, 6, 0));
		CreateCollision(Vector3(-8.4, 4, 0));
		CreateCollision(Vector3(7.5, 6, 0));
		CreateCollision(Vector3(7.5, 4, 0));*/
	}

	void AddDestructibles()
	{
		//CreateDestructible(0, Vector3(-1.4f, -2.6f, 0));
		//CreateDestructible(1, Vector3(-2.2f, -2.2f, 0));
		////CreateDestructible(2, Vector3(-3.2f, -2.2f, 0));
		////CreateDestructible(0, Vector3(-4.3f, -2.0f, 0));
		//CreateDestructible(2, Vector3(-7.1f, -2.1f, 0));
		////CreateDestructible(0, Vector3(-8.5f, -2.5f, 0));
		//CreateDestructible(0, Vector3(-7.1f, -10.2f, 0));
		////CreateDestructible(1, Vector3(-8.3f, -10.1f, 0));
		//CreateDestructible(0, Vector3(-9.6f, -10.1f, 0));
		////CreateDestructible(2, Vector3(-8.7f, -11.0f, 0));
		//CreateDestructible(1, Vector3(-9.4f, -11.3f, 0));
		////CreateDestructible(2, Vector3(-8.0f, -11.5f, 0));
		//CreateDestructible(2, Vector3(-7.1f, -11.3f, 0));
		//CreateDestructible(2, Vector3(-9.4f, -12.3f, 0));
		////CreateDestructible(1, Vector3(-8.3f, -12.5f, 0));
		////CreateDestructible(1, Vector3(-7.1f, -11.3f, 0));
		//CreateDestructible(2, Vector3(-7.1f, -12.5f, 0));
		////CreateDestructible(0, Vector3(-1.4f, -12.0f, 0));

		////CreateDestructible(0, Vector3(0.5f, -2.6f, 0));
		//CreateDestructible(1, Vector3(1.5f, -2.2f, 0));
		////CreateDestructible(2, Vector3(2.4f, -2.2f, 0));
		//CreateDestructible(0, Vector3(3.6f, -2.0f, 0));
		//CreateDestructible(2, Vector3(6.3f, -2.1f, 0));
		//CreateDestructible(0, Vector3(7.6f, -2.5f, 0));
		////CreateDestructible(0, Vector3(6.3f, -10.2f, 0));
		////CreateDestructible(1, Vector3(7.5f, -10.1f, 0));
		//CreateDestructible(0, Vector3(8.9f, -10.1f, 0));
		//CreateDestructible(2, Vector3(7.9f, -11.0f, 0));
		////CreateDestructible(1, Vector3(8.7f, -11.3f, 0));
		//CreateDestructible(2, Vector3(7.1f, -11.5f, 0));
		////CreateDestructible(2, Vector3(6.3f, -11.3f, 0));
		//CreateDestructible(2, Vector3(8.5f, -12.3f, 0));
		//CreateDestructible(1, Vector3(7.5f, -12.5f, 0));
		////CreateDestructible(1, Vector3(6.3f, -11.3f, 0));
		////CreateDestructible(2, Vector3(6.3f, -12.5f, 0));
		////CreateDestructible(0, Vector3(0.8f, -12.0f, 0));

		//CreateDestructible(0, Vector3(-1.4f, 1.8f, 0));
		//CreateDestructible(1, Vector3(-2.2f, 1.2f, 0));
		////CreateDestructible(2, Vector3(-3.2f, 1.2f, 0));
		//CreateDestructible(0, Vector3(-4.3f, 1.0f, 0));
		////CreateDestructible(2, Vector3(-7.1f, 1.1f, 0));
		//CreateDestructible(0, Vector3(-8.5f, 1.5f, 0));
		////CreateDestructible(0, Vector3(-7.1f, 9.2f, 0));
		//CreateDestructible(1, Vector3(-8.3f, 9.1f, 0));
		////CreateDestructible(0, Vector3(-9.6f, 9.1f, 0));
		////CreateDestructible(2, Vector3(-8.7f, 10.0f, 0));
		////CreateDestructible(1, Vector3(-9.4f, 10.3f, 0));
		//CreateDestructible(2, Vector3(-8.0f, 10.5f, 0));
		//CreateDestructible(2, Vector3(-7.1f, 10.3f, 0));
		////CreateDestructible(2, Vector3(-9.4f, 11.3f, 0));
		////CreateDestructible(1, Vector3(-8.3f, 11.5f, 0));
		////CreateDestructible(1, Vector3(-7.1f, 10.3f, 0));
		//CreateDestructible(2, Vector3(-7.1f, 11.5f, 0));
		//CreateDestructible(0, Vector3(-1.4f, 11.0f, 0));
		////CreateDestructible(0, Vector3(0, 0, 0));

		//CreateDestructible(0, Vector3(0.5f, 1.6f, 0));
		////CreateDestructible(1, Vector3(1.5f, 1.2f, 0));
		//CreateDestructible(2, Vector3(2.4f, 1.2f, 0));
		//CreateDestructible(0, Vector3(3.6f, 1.0f, 0));
		////CreateDestructible(2, Vector3(6.3f, 1.1f, 0));
		//CreateDestructible(0, Vector3(7.6f, 1.5f, 0));
		//CreateDestructible(0, Vector3(6.3f, 9.2f, 0));
		////CreateDestructible(1, Vector3(7.5f, 9.1f, 0));
		////CreateDestructible(0, Vector3(8.9f, 9.1f, 0));
		////CreateDestructible(2, Vector3(7.9f, 9.0f, 0));
		//CreateDestructible(1, Vector3(8.7f, 10.3f, 0));
		////CreateDestructible(2, Vector3(7.1f, 10.5f, 0));
		//CreateDestructible(2, Vector3(6.3f, 10.3f, 0));
		//CreateDestructible(2, Vector3(8.5f, 11.3f, 0));
		////CreateDestructible(1, Vector3(7.5f, 11.5f, 0));
		//CreateDestructible(1, Vector3(6.3f, 10.3f, 0));
		////CreateDestructible(2, Vector3(6.3f, 11.5f, 0));
		////CreateDestructible(0, Vector3(0.8f, 11.0f, 0));
	}

}


void Server::SetupWorld()
{
	//spawn some random mice
	//CreateRandomMice( 10 );
	
	//spawn more random mice!
	//CreateRandomMice( 10 );

	//AddDestructibles();
	//AddCollisions();
	//CreateVan();
	/*ector3 lampPos(5.f, 0.f, 0.f);
	GameObjectPtr goLamp = GameObjectRegistry::sInstance->CreateGameObject('LAMP');
	goLamp->SetLocation(lampPos);
	World::sInstance->AddGameObject(goLamp);*/
}



void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnCats();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();

}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{
	
	int playerId = inClientProxy->GetPlayerId();
	
	ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName() );
	SpawnCatForPlayer( playerId );
}

void Server::SpawnCatForPlayer( int inPlayerId )
{
	RoboCatPtr cat = std::static_pointer_cast< RoboCat >( GameObjectRegistry::sInstance->CreateGameObject( 'RCAT' ) );
	cat->SetColor( ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	cat->SetPlayerId( inPlayerId );
	//gotta pick a better spawn location than this...
	cat->SetLocation( Vector3( 1.f - static_cast< float >( inPlayerId ), 0.f, 0.f ) );

}

void Server::SpawnVan() {
	GameObjectPtr go = GameObjectRegistry::sInstance->CreateGameObject('AVAN');
	go->SetLocation(Vector3(1, 1, 0));
	go->SetRotation(RoboMath::GetRandomFloat() * 360);
}

void Server::TurnPlayerToJustice(int inPlayerId)
{
	
	RoboCatPtr cat = GetCatForPlayer(inPlayerId);
	cat->SetDoesWantToDie(true);
	//cat->team = RoboCat::Team::Justice;

	JusticePtr justice = std::static_pointer_cast< Justice >(GameObjectRegistry::sInstance->CreateGameObject('RJUS'));
	justice->SetLocation(cat->GetLocation());
	justice->SetColor(ScoreBoardManager::sInstance->GetEntry(inPlayerId)->GetColor());
	justice->SetPlayerId(inPlayerId);
	//justice->SetLocation(Vector3(1.f - static_cast< float >(inPlayerId), 0.f, 0.f));
}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's cat
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	ScoreBoardManager::sInstance->RemoveEntry( playerId );
	RoboCatPtr cat = GetCatForPlayer( playerId );
	
	auto removeIter = std::find(NetworkManagerServer::sInstance->mPlayersReady.begin(), NetworkManagerServer::sInstance->mPlayersReady.end(), inClientProxy->mReady);
	if (removeIter != NetworkManagerServer::sInstance->mPlayersReady.end()) {
		NetworkManagerServer::sInstance->mPlayersReady.erase(removeIter);
	}

	if( cat )
	{
		cat->SetDoesWantToDie( true );
	}
}

VanPtr Server::FindVan() {
	if (!gameOver) {
		const auto& gameObjects = World::sInstance->GetGameObjects();
		for (int i = 0, c = gameObjects.size(); i < c; ++i)
		{
			GameObjectPtr go = gameObjects[i];
			Van* van = go->GetAsVan();
			if (van != nullptr) {
				return std::static_pointer_cast< Van >(go);
			}
		}
		return nullptr;
	}

}

JusticePtr Server::FindJustice() {
	if (!gameOver) {
		const auto& gameObjects = World::sInstance->GetGameObjects();
		for (int i = 0, c = gameObjects.size(); i < c; ++i)
		{
			GameObjectPtr go = gameObjects[i];
			Justice* justice = go->GetAsJustice();
			if (justice != nullptr) {
				return std::static_pointer_cast< Justice >(go);
			}
		}
		return nullptr;
	}
	
}

RoboCatPtr Server::GetCatForPlayer( int inPlayerId )
{
	//run through the objects till we find the cat...
	//it would be nice if we kept a pointer to the cat on the clientproxy
	//but then we'd have to clean it up when the cat died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];
		RoboCat* cat = go->GetAsCat();
		if( cat && cat->GetPlayerId() == inPlayerId )
		{
			return std::static_pointer_cast< RoboCat >( go );
		}
	}

	return nullptr;

}