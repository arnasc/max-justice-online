#include <RoboCatServerPCH.h>

namespace
{
	// IMPORTANT: RESPAWN DELAY?
	const float kRespawnDelay = 3.f;
}

ClientProxy::ClientProxy( const SocketAddress& inSocketAddress, const string& inName, int inPlayerId, int inTeam):
mSocketAddress( inSocketAddress ),
mName( inName ),
mPlayerId( inPlayerId ),
mDeliveryNotificationManager( false, true ),
mIsLastMoveTimestampDirty( false ),
mTimeToRespawn( 0.f ),
mTeam(inTeam)
{
	UpdateLastPacketTime();
}


void ClientProxy::UpdateLastPacketTime()
{
	mLastPacketFromClientTime = Timing::sInstance.GetTimef(); 
}

void	ClientProxy::HandleCatDied()
{
	mTimeToRespawn = Timing::sInstance.GetFrameStartTime() + kRespawnDelay;
}

void	ClientProxy::RespawnCatIfNecessary()
{
	if( mTimeToRespawn != 0.f && Timing::sInstance.GetFrameStartTime() > mTimeToRespawn )
	{
		static_cast< Server* > ( Engine::sInstance.get() )->SpawnCatForPlayer( mPlayerId );
		mTimeToRespawn = 0.f;
	}
}

void	ClientProxy::TurnPlayerToJustice()
{
	mTeam = 1;
	static_cast< Server* > (Engine::sInstance.get())->TurnPlayerToJustice(mPlayerId);
}
