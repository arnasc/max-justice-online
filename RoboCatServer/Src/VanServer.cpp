#include <RoboCatServerPCH.h>
// IMPORTANT: BULLET COOLDOWN
VanServer::VanServer()
{}


void VanServer::HandleDying()
{
	//NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

bool VanServer::HandleCollisionWithCat(RoboCat* inCat)
{
	auto server = static_cast< Server*> (Engine::sInstance.get());
	if (!server->gameOver) {
		JusticePtr justice = static_cast< Server* > (Engine::sInstance.get())->FindJustice();
		if (justice != nullptr) {
			justice->SetDoesWantToDie(true);
			server->gameOver = true;
			NetworkManagerServer::sInstance->SendSpriteAllClients(justice->GetLocation(), NetworkManagerServer::Explosion);
		}
		return false;
	}
	return false;
}

