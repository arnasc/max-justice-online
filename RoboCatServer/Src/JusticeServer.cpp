#include <RoboCatServerPCH.h>
#include <SQLConn.h>
// IMPORTANT: BULLET COOLDOWN
JusticeServer::JusticeServer() :
	mCatControlType( ESJT_Human ),
	mTimeOfNextShot( 0.f ),
	mTimeBetweenShots( 0.2f )
{}

void JusticeServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

void JusticeServer::Update()
{
	Justice::Update();
	
	if (isSkiddingLeft) {
		if (Timing::sInstance.GetTimef() > lastSkidMark + .005f) {
			GameObjectPtr go = GameObjectRegistry::sInstance->CreateGameObject('SKDL');
			go->SetLocation(GetLocation());
			go->SetRotation(GetRotation());
			lastSkidMark = Timing::sInstance.GetTimef();
		}
	}
	else if (isSkiddingRight) {
		if (Timing::sInstance.GetTimef() > lastSkidMark + .005f) {
			GameObjectPtr go = GameObjectRegistry::sInstance->CreateGameObject('SKDR');
			go->SetLocation(GetLocation());
			go->SetRotation(GetRotation());
			lastSkidMark = Timing::sInstance.GetTimef();
		}
	}

	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();
	float oldRotation = GetRotation();

	//are you controlled by a player?
	//if so, is there a move we haven't processed yet?
	if( mCatControlType == ESJT_Human)
	{
		ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy( GetPlayerId() );
		if( client )
		{
			MoveList& moveList = client->GetUnprocessedMoveList();
			for( const Move& unprocessedMove : moveList )
			{
				const InputState& currentState = unprocessedMove.GetInputState();

				float deltaTime = unprocessedMove.GetDeltaTime();

				ProcessInput( deltaTime, currentState );
				SimulateMovement( deltaTime );

				//LOG( "Server Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f", unprocessedMove.GetTimestamp(), deltaTime, GetRotation() );

			}

			moveList.Clear();
		}
	}
	else
	{
		//do some AI stuff
		SimulateMovement( Timing::sInstance.GetDeltaTime() );
	}

	HandleShooting();

	if( !RoboMath::Is2DVectorEqual( oldLocation, GetLocation() ) ||
		!RoboMath::Is2DVectorEqual( oldVelocity, GetVelocity() ) ||
		oldRotation != GetRotation() )
	{
		NetworkManagerServer::sInstance->SetStateDirty( GetNetworkId(), ECRS_Pose );
	}

}

void JusticeServer::HandleShooting()
{
	//float time = Timing::sInstance.GetFrameStartTime();
	//if( mIsShooting && Timing::sInstance.GetFrameStartTime() > mTimeOfNextShot )
	//{
	//	//not exact, but okay
	//	mTimeOfNextShot = time + mTimeBetweenShots;

	//	//fire!
	//	YarnPtr yarn = std::static_pointer_cast< Yarn >( GameObjectRegistry::sInstance->CreateGameObject( 'YARN' ) );
	//	yarn->InitFromShooter( this );
	//}
}


void JusticeServer::TurnToJustice(int inPlayerToJusticeId) {
	//team = Team::Justice;
	//NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), ECRS_Team);
	//NetworkManagerServer::sInstance->GetClientProxy(inPlayerToJusticeId)->TurnPlayerToJustice();
	SetDoesWantToDie(true);
	
	ClientProxyPtr clientProxy = NetworkManagerServer::sInstance->GetClientProxy(GetPlayerId());
	if (clientProxy)
	{
		clientProxy->TurnPlayerToJustice();
	}
}

void JusticeServer::TakeDamage( int inDamagingPlayerId )
{
	ClientProxyPtr player = NetworkManagerServer::sInstance->GetClientProxy(inDamagingPlayerId);
	int shootingPlayersTeam = player->GetTeam();

	//if (team == shootingPlayersTeam) {
	//	return;
	//}


	mHealth--;
	if( mHealth <= 0.f )
	{
		//score one for damaging player...
		ScoreBoardManager::sInstance->IncScore( inDamagingPlayerId, 1 );

		//and you want to die
		SetDoesWantToDie( true );

		//tell the client proxy to make you a new cat
		ClientProxyPtr clientProxy = NetworkManagerServer::sInstance->GetClientProxy( GetPlayerId() );
		if( clientProxy )
		{
			clientProxy->HandleCatDied();
		}
	}

	//tell the world our health dropped
	NetworkManagerServer::sInstance->SetStateDirty( GetNetworkId(), ECRS_Health );
}

bool JusticeServer::HandleCollisionWithCat(RoboCat* inCat)
{

	if (inCat) {
		if (!inCat->DoesWantToDie()) {
			NetworkManagerServer::sInstance->mPlayersAlive--;
			if (NetworkManagerServer::sInstance->mPlayersAlive == 2) {

				VanPtr van = static_cast< Server* > (Engine::sInstance.get())->FindVan();
				if (van != nullptr) {
					van->SetLocation(Vector3(1, 1, 0));
					NetworkManagerServer::sInstance->SetStateDirty(van->GetNetworkId(), Van::EVanReplicationState::EMRS_Pose);
					NetworkManagerServer::sInstance->SendSuddenDeathAllClients();
				}
			}

			if (NetworkManagerServer::sInstance->mPlayersAlive == 1) {
				//SQLConn::UpdateTable();
			}
			/*std::vector<bool>::iterator findIterator = std::find(NetworkManagerServer::sInstance->mPlayersAlive.begin(), NetworkManagerServer::sInstance->mPlayersAlive.begin(), true);
			if (findIterator != NetworkManagerServer::sInstance->mPlayersAlive.end()) {
				NetworkManagerServer::sInstance->mPlayersAlive.erase(NetworkManagerServer::sInstance->mPlayersAlive.begin());
			}

			if (NetworkManagerServer::sInstance->mPlayersAlive.size() == 2) {
				static_cast< Server* > (Engine::sInstance.get())->SpawnVan();
			}*/
		}
	}


	inCat->SetDoesWantToDie(true);

	NetworkManagerServer::sInstance->SendSpriteAllClients(inCat->GetLocation(), NetworkManagerServer::JusticePop);
	NetworkManagerServer::sInstance->SendSpriteAllClients(inCat->GetLocation(), NetworkManagerServer::Explosion);
	
	return false;
}

